package cn.boodqian.utils;

import junit.framework.Assert;
import android.os.Handler;
import android.os.Message;

public class MessageHelper {
	public static void sendMessage(Handler handler, int what) {
		sendMessage(handler, what, 0, 0, null);
	}
		
	public static void sendMessage(Handler handler, int what, Object obj) {
		sendMessage(handler, what, 0, 0, obj);
	}
	
	public static void sendMessage(Handler handler, int what, int arg1, int arg2) {
		sendMessage(handler, what, arg1, arg2, null);
	}
	
	public static void sendMessage(Handler handler, int what, int arg1, int arg2, Object obj) {
		Assert.assertNotNull(handler);
		
		Message msg = new Message();
		msg.what = what;
		msg.arg1 = arg1;
		msg.arg2 = arg2;
		msg.obj = obj;
		handler.sendMessage(msg);
	}
}

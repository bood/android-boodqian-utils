package cn.boodqian.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import org.xbill.DNS.*;

import junit.framework.Assert;

public class GAERequest {
	public static class TitleMessageHandler extends Handler {
		private Activity mActivity;
		private String originalTitle;
		public TitleMessageHandler(Activity activity, String title) {
			Assert.assertNotNull(activity);
			Assert.assertNotNull(title);
			this.mActivity = activity;
			this.originalTitle = title;
		}
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what) {
			case GAERequest.MESSAGE_DNS:
				mActivity.setTitle(R.string.gae_message_dns);
				break;
			case GAERequest.MESSAGE_QUERY:
				mActivity.setTitle(
						String.format( mActivity.getString(R.string.gae_message_query), msg.arg1, msg.arg2));
				break;
			case GAERequest.MESSAGE_DONE_OK:
				mActivity.setTitle(R.string.gae_message_success);
				mActivity.setTitle(originalTitle);
				break;
			case GAERequest.MESSAGE_DONE_ERR:
				mActivity.setTitle(R.string.gae_message_fail);
				mActivity.setTitle(originalTitle);
				break;
			case GAERequest.MESSAGE_RUNNING:
				Toast.makeText(
						mActivity,
						mActivity.getString(R.string.gae_message_running),
						Toast.LENGTH_LONG).show();
				break;
			}
		}
	}
	
	private class TrustAllHostNameVerifier implements HostnameVerifier {

		public boolean verify(String hostname, SSLSession session) {
			return true;
		}

	}
	//private final static String[] m_IPs = new String[] {"203.208.46.1","203.208.46.2","203.208.46.3","203.208.46.4","203.208.46.5","203.208.46.6","203.208.46.7","203.208.46.8"};
	private final static String[] mHosts = new String[] {/*"203.208.47.81","203.208.46.240","203.208.45.208","203.208.47.82","203.208.46.241","203.208.45.209"*/};
	private final static String[] mSecureHostDomains = new String[] {"www.google.com","mail.google.com","www.l.google.com","mail.l.google.com"};
	private final static String [] mDnsServers = new String[] {null, "114.114.114.114", "8.8.8.8"};
	private static HashSet<String> mBlackList = new HashSet<String>(
			Arrays.asList( new String[] { "203.98.7.65","159.106.121.75","159.24.3.173","46.82.174.68","78.16.49.15","59.24.3.173","243.185.187.39","243.185.187.30","8.7.198.45","37.61.54.158","93.46.8.89" } ));
	private static LinkedHashSet<String> mSecureHosts = new LinkedHashSet<String>();
	private final static String[] mBackupHosts = new String[] {"173.194.32.21","173.194.32.22","173.194.32.53","173.194.32.54","173.194.72.103","173.194.72.104","173.194.72.105","173.194.72.106","173.194.72.147","173.194.72.17","173.194.72.18","173.194.72.19","173.194.72.83","173.194.72.99","72.14.203.17","72.14.203.18","72.14.203.19","72.14.203.83","74.125.128.103","74.125.128.104","74.125.128.105","74.125.128.106","74.125.128.147","74.125.128.17","74.125.128.18","74.125.128.19","74.125.128.83","74.125.128.99","74.125.130.17","74.125.130.18","74.125.130.19","74.125.130.83","74.125.134.17","74.125.134.18","74.125.134.19","74.125.134.83","74.125.135.17","74.125.135.18","74.125.135.19","74.125.135.83","74.125.225.85","74.125.225.86","74.125.235.21","74.125.235.213","74.125.235.214","74.125.235.22","74.125.235.85","74.125.235.86","74.125.65.17","74.125.65.18","74.125.65.19","74.125.65.83"};
	private final static String mLocalIP = "10.0.2.2";
	private final static int mLocalPort = 8888;
	private int mBunchSize = 3;
	private int mMaxBunch = 7;
	private long mTimeout = 6500;
	private boolean mSkipNormal = true;
	public static Integer mLastIPIndex = 0;
	private String hostname;
	private String lasterror = "";
	public final static ArrayList<String[]> gEffectiveHosts = new ArrayList<String[]>();
	private final ArrayList<String[]> mEffectiveHosts = new ArrayList<String[]>();
	private boolean mUseLocalHost = false;
	private Handler mMonitorHandler;
	private Handler mPrivateHandler = new Handler();
	private Boolean mRunning = false;
	
	public final static int MESSAGE_DNS = 1000;
	public final static int MESSAGE_QUERY = 1001;
	public final static int MESSAGE_DONE_OK = 1002;
	public final static int MESSAGE_DONE_ERR = 1003;
	public final static int MESSAGE_RUNNING = 1004;
	public final static int MESSAGE_START = 1005;
	
	public GAERequest(String hostname) {
		this(hostname, null, false);
	}
		
	public GAERequest(String hostname, Handler handler) {
		this(hostname, handler, false);
	}
	
	public GAERequest(String hostname, Handler handler, boolean useLocalHost) {
		Assert.assertTrue(hostname!=null && hostname.length()>0);
		
		this.hostname = hostname;
		this.mUseLocalHost = useLocalHost;
		this.mMonitorHandler = handler;
	}
	
	public void setBunchOpt(int bunchSize, int maxBunch) {
		Assert.assertTrue("bunch size must positive", bunchSize > 0 );
		Assert.assertTrue("max bunch must positive", maxBunch > 0 );
		mBunchSize = bunchSize;
		mMaxBunch = maxBunch;
	}
	
	public long setTimeout(long timeout) {
	    long old = mTimeout;
	    Assert.assertTrue(timeout > 1000); // At least one second
	    mTimeout = timeout;
	    return old;
	}
	
	public boolean setSkipNormal(boolean skip) {
	    boolean old = mSkipNormal;
	    mSkipNormal = skip;
	    return old;
	}
	
	private void updateEffectiveHosts() {
		queryDNS();
		
		gEffectiveHosts.clear();
		for( String host : mSecureHosts ) {
			String [] hostProtocol = new String[] { host, "https" };
			gEffectiveHosts.add(hostProtocol);
		}
		for( String host : mHosts ) {
			String [] hostProtocol = new String[] { host, "http" };
			gEffectiveHosts.add(hostProtocol);
		}
	}
	
	private void updateLocalEffectiveHosts() {
		mEffectiveHosts.clear();
		if( mUseLocalHost ) {
			mEffectiveHosts.add(new String[] { mLocalIP, "http" });
		} else {
			// Try normal way first
		    if( ! mSkipNormal )
		        mEffectiveHosts.add(new String[]{hostname, "http"});
			mEffectiveHosts.addAll(gEffectiveHosts);
		}
	}
	
	// synchronize should be done by caller
	private boolean queryDNS() {
		mSecureHosts.clear();
		for( String host : mSecureHostDomains ) {
			for( String server : mDnsServers ) {
				Log.i( host + " : " + ((server==null)?"local":server) );
				try
				{
				    SimpleResolver res = null;
				    if (server != null)
				        res = new SimpleResolver(server);
				    else
				        res = new SimpleResolver();
				    Name name = Name.fromString(host, Name.root);
				    Record rec = Record.newRecord(name, Type.A, DClass.IN);
				    org.xbill.DNS.Message query = org.xbill.DNS.Message.newQuery(rec);
				    org.xbill.DNS.Message response = res.send(query);
				    for( Record record : response.getSectionArray(Section.ANSWER) ) {
				        if( record.getType() != Type.A ) continue;
				        String ip = record.rdataToString();
				        if( mBlackList.contains(ip) ) {
				            Log.w(String.format("Black list IP found, host:%s dns:%s ip:%s", host, server, ip));
				            continue;
				        }
				        if( mSecureHosts.add(ip) ) Log.i(ip);
				    }
				}
				catch (Exception e)
				{
					Log.e(e.toString());
				}
			}
		}
        
        mSecureHosts.addAll( Arrays.asList(mBackupHosts) );
        
		return true;
	}
	
	public String doGet(String path) {
		return doQuery(path, null, "GET");
	}
	
	public String doPost(String path, String data) {
		return doQuery(path, data, "POST");
	}
	
	private void getBunch(String []hosts, String []protocols, int bunchIndex) {
		Assert.assertTrue(hosts.length >= mBunchSize);
		Assert.assertTrue(protocols.length >= mBunchSize);
		int base = bunchIndex * mBunchSize;
		for( int i = 0; i < mBunchSize; i++ ) {
			if( base + i < mEffectiveHosts.size() ) {
				hosts[i] = mEffectiveHosts.get(base+i)[0];
				protocols[i] = mEffectiveHosts.get(base+i)[1];
			} else {
				hosts[i] = null;
				protocols[i] = null;
			}
		}
	}
	
	// path: "/airdata"
	private String doQuery(final String path, final String data, final String method) {
		Assert.assertTrue(path, path.startsWith("/"));
		
		synchronized(mRunning) {
			if( mRunning ) {
				Log.i("Request to "+path+" already running");
				if(mMonitorHandler!=null)
					MessageHelper.sendMessage(mMonitorHandler, MESSAGE_RUNNING);
				return "";
			}

			if(mMonitorHandler!=null)
				MessageHelper.sendMessage(mMonitorHandler, MESSAGE_START);
			mRunning = true;
		}
		
		Log.i("Requesting="+path);
		if( mUseLocalHost && data != null ) Log.i("Post Data="+data);
		
		// Put outside of sync, since it's possible someone else is querying DNS silently
		if(mMonitorHandler!=null)
			MessageHelper.sendMessage(mMonitorHandler, MESSAGE_DNS);
		
		synchronized(gEffectiveHosts) {
			if( gEffectiveHosts.size() == 0 ) {
				updateEffectiveHosts();
			}
		}
		
		synchronized(mEffectiveHosts) {
			if( mEffectiveHosts.size() == 0 ) {
				updateLocalEffectiveHosts();
			}
		}
		
		int bunchNumber = ( mEffectiveHosts.size() + mBunchSize - 1 )/mBunchSize;
		if( bunchNumber > mMaxBunch ) bunchNumber = mMaxBunch;
		final String [] hosts = new String[mBunchSize];
		final String [] protocols = new String[mBunchSize];
		String ret = null;
		for(int bunch=0;bunch<bunchNumber;bunch++) {
			getBunch(hosts, protocols, bunch);

			// Try last success host first
			synchronized(mLastIPIndex) {
				if( bunch == 0 && mLastIPIndex < bunchNumber ) {
					getBunch(hosts, protocols, mLastIPIndex);
				} else if( bunch == mLastIPIndex ) {
					getBunch(hosts, protocols, 0);
				}
			}
			
			Log.i( String.format("[%s] Trying bunch %d of %d", path, bunch+1, bunchNumber ) );
			if(mMonitorHandler!=null)
				MessageHelper.sendMessage(mMonitorHandler, MESSAGE_QUERY, bunch+1, bunchNumber);
			
			
			final String [] states = new String[mBunchSize];
			final Thread [] threads = new Thread[mBunchSize];
			for( int i = 0; i < mBunchSize; i++ ) {
				states[i] = "";
				if( protocols[i] == null || hosts[i] == null ) {
					continue;
				}
				
				final String host = hosts[i];
				final String protocol = protocols[i];
				final int index = i;
				threads[i] = new Thread() {
					@Override
					public void run() {
						StringBuilder builder = new StringBuilder();
						URL url;
						try {
							url = new URL(protocol, host, mUseLocalHost?8888:-1, path);
							Log.i("Trying "+url.toString());

							HttpURLConnection connection = (HttpURLConnection) url.openConnection();
							if( protocol.equalsIgnoreCase("https") ) {
								HttpsURLConnection https = (HttpsURLConnection)connection;
								https.setHostnameVerifier(new TrustAllHostNameVerifier());
							}
							connection.setRequestMethod(method);
							if( method.equals("POST") ) // Do NOT set this in GET mode, or it will force to use POST
								connection.setDoOutput(true);
							connection.setRequestProperty("Host", hostname);
							connection.setConnectTimeout(5000);

							if( data != null && method.compareTo("POST")==0 ) {
								OutputStream output = null;
								try {
									output = connection.getOutputStream();
									output.write(data.getBytes("UTF-8"));
								} finally {
									if (output != null) {
										try {
											output.close();
										} catch (IOException e) {
											// ignored
										}
									}
								}
							}
							BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

							String line;
							while ((line = reader.readLine()) != null) {
								builder.append(line);
							}
							synchronized(states) {
								states[index] = builder.toString();
							}
						} catch (MalformedURLException e) {
							builder.setLength(0);
							lasterror = e.getLocalizedMessage();
						} catch (IOException e) {
							builder.setLength(0);
							e.printStackTrace();
							lasterror = e.getLocalizedMessage();
						} catch (Exception e) {
							builder.setLength(0);
							e.printStackTrace();
							lasterror = e.toString();
						} finally {
							if( states[index].length() == 0 ) {
								Log.w(lasterror);
							} else {
								Log.i( String.format("[%s] %s %s success", path, protocol, host ));
							}
						}
					}
				};
				threads[i].start();
			}
			
			// Checking all results
			long start = new Date().getTime();
			while(true) {
				for( int i = 0; i < mBunchSize; i++ ) {
					synchronized(states) {
						if( states[i].length() > 0 ) {
							Log.i( String.format("[%s] Index %d of bunch %d success", path, i, bunch ));
							ret = states[i];
							break;
						}
					}
				}
				if( ret != null ) break;
				long now = new Date().getTime();
				if( now - start > mTimeout ) {
					Log.i( String.format("[%s] Bunch %d timeouted", path, bunch ));
					break;
				}
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					Log.e(e.getLocalizedMessage());
				}
			}
			
			if( ret != null ) break;
		}
		
		if( ret != null ) {
			if(mMonitorHandler!=null)
				MessageHelper.sendMessage(mMonitorHandler, MESSAGE_DONE_OK, ret);
			if( mUseLocalHost ) Log.i("Response="+ret);

			// Find out the success IP index
			synchronized(mLastIPIndex) {
				for( int j=0; j<bunchNumber; j++ ) {
					int base = mBunchSize*j;
					boolean match = true;
					for ( int i = 0; i < mBunchSize; i++ ) {
						if( hosts[i] != null && !hosts[i].equals(mEffectiveHosts.get(base+i)[0]))
							match = false;
					}
					if( match ) {
						mLastIPIndex = j;
						break;
					}
				}
			}
		} else {
			if(mMonitorHandler!=null)
				MessageHelper.sendMessage(mMonitorHandler, MESSAGE_DONE_ERR);
		}
		
		Log.i("Done "+path);
		
		synchronized(mRunning) {
			mRunning = false;
		}
				
		return (ret!=null)?ret:"";
	}
	
	public String getLastError() {
		return lasterror;
	}
}

package cn.boodqian.utils;

public final class Log {
	private final static String TAG = "AirReport";
	public final static int VERBOSE = android.util.Log.VERBOSE;
	public final static int DEBUG = android.util.Log.DEBUG;
	public final static int INFO = android.util.Log.INFO;
	public final static int WARN = android.util.Log.WARN;
	public final static int ERROR = android.util.Log.ERROR;
	
	public final static boolean isLoggable(int level) {
		return android.util.Log.isLoggable(TAG, level);
	}
	public final static void v(String msg) {
		if(android.util.Log.isLoggable(TAG, android.util.Log.VERBOSE)) {
			StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
			msg = "["+stackTraceElements[3].getClassName()+"::"+stackTraceElements[3].getMethodName() + "]" + msg;
			android.util.Log.v(TAG, msg);
		}
	}
	public final static void d(String msg) {
		if(android.util.Log.isLoggable(TAG, android.util.Log.DEBUG)) {
			StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
			msg = "["+stackTraceElements[3].getClassName()+"::"+stackTraceElements[3].getMethodName() + "]" + msg;
			android.util.Log.d(TAG, msg);
		}
	}
	public final static void i(String msg) {
		if(android.util.Log.isLoggable(TAG, android.util.Log.INFO)) {
			StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
			msg = "["+stackTraceElements[3].getClassName()+"::"+stackTraceElements[3].getMethodName() + "]" + msg;
			android.util.Log.i(TAG, msg);
		}
	}
	public final static void w(String msg) {
		if(android.util.Log.isLoggable(TAG, android.util.Log.WARN)) {
			StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
			msg = "["+stackTraceElements[3].getClassName()+"::"+stackTraceElements[3].getMethodName() + "]" + msg;
			android.util.Log.w(TAG, msg);
		}
	}
	public final static void e(String msg) {
		if(android.util.Log.isLoggable(TAG, android.util.Log.ERROR)) {
			StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
			msg = "["+stackTraceElements[3].getClassName()+"::"+stackTraceElements[3].getMethodName() + "]" + msg;
			android.util.Log.e(TAG, msg);
		}
	}
	public final static void wtf(String msg) {
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		msg = "["+stackTraceElements[3].getClassName()+"::"+stackTraceElements[3].getMethodName() + "]" + msg;
		android.util.Log.wtf(TAG, msg);
	}
}
